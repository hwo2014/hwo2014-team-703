(ns hwo2014bot.core
  (:require [clojure.data.json :as json])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))

;---------------------------------------------------------------;
(def recThrArray (atom 0))
;
(def carColor (atom ""))
(def straightDefaultVal (atom 0.65))
(def currIndex (atom 0))
(def turboOn (atom false))
(def turboAv (atom false))
(def turboTicks (atom 0))
(def turboFactor (atom 0))

(def NrStraight (atom 0))
(def NrRadius100 (atom 0))
(def NrRadius50 (atom 0))

;---------------------------------------------------------------;
;---------------------------------------------------------------;
(defn- json->clj [string]
  (json/read-str string :key-fn keyword))
(defn send-message [channel message]
  (enqueue channel (json/write-str message)))
(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (System/exit 1)))))
(defn log-msg [msg]
  (case (:msgType msg)
    "join" (do (println "Joined") (println (:data msg)))
    "gameStart" (println "Race started")
    "crash" (do (println "Someone crashed") (println (:data msg)))
    "lapFinished" (do (println "Lap finished") (println (:data msg)))
    "spawn" (do (println "spawn") (println (:data msg)))
    "yourCar" (println "Your car")
    "gameEnd" (do (println "Race ended")(println @NrStraight @NrRadius100 @NrRadius50))
    "gameInit" (println "Race init")
    "error" (println (str "ERROR: " (:data msg)))
    "turboAvailable" (println "Turbo charged")
    :noop))
(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))
(defn- create-ping []
  {:msgType "ping" :data "ping"})


;---------------------------------------------------------------;
;---------------------------------------------------------------;
(defn- carData [msgs]
  (loop [x 0]
    (let [msg (nth (:data msgs) x)
          ident (= (:color (:id msg)) @carColor)]
      (if ident msg (recur (+ x 1)))
  )))
;---------------------------------------------------------------;
(defn- carPosition [msgs]
  (:piecePosition (carData msgs)))
;---------------------------------------------------------------;
(defn- carAngle [msgs]
  (:angle (carData msgs)))
;---------------------------------------------------------------;
(defn- addThrIndex! [index type value angle switch]
  (swap! recThrArray conj {:index index :type type :value value :angle angle :switch switch}))
;---------------------------------------------------------------;
(defn- fillThrottleArray [pieces]
  (do
    ;Analyze tack composition
    (doseq [piece pieces]
      (if (:radius piece)
          (if (< (:radius piece) 60)
            (swap! NrRadius50 inc)
            (swap! NrRadius100 inc))
          (swap! NrStraight inc))
    )
    ;Set default speed for straight parts
    (if (>= @NrStraight (+ @NrRadius100 @NrRadius50))
      (reset! straightDefaultVal 0.9)
      (if (> @NrRadius50 @NrRadius100)
        (reset! straightDefaultVal 0.43)))
    ;Fill the throttle value array
    (loop [x 0]
      (when (< x (count pieces))
        (let [piece (nth pieces x)
              radius (:radius piece)
              angle (:angle piece)
              switch (:switch piece)
             ]
          (if radius
              (if (< radius 60)
                (addThrIndex! x :R 0.4 angle switch)
                (addThrIndex! x :R 0.65 angle switch))
              (addThrIndex! x :S @straightDefaultVal 0 switch))
          )
        (recur (+ x 1)))
    )
  ))
;---------------------------------------------------------------;
(defn- static-thr-calc [pieces]
  (fillThrottleArray pieces))
;---------------------------------------------------------------;
(defn- realtime-straight-turbo [throttle]
  (let [index1 (:index throttle)
        index2 (mod (+ index1 1) (count @recThrArray))
        index3 (mod (+ index1 2) (count @recThrArray))
        index4 (mod (+ index1 3) (count @recThrArray))
        straight1 (= (:type (nth @recThrArray index1)) :S)
        straight2 (= (:type (nth @recThrArray index2)) :S)
        straight3 (= (:type (nth @recThrArray index3)) :S)
        straight4 (= (:type (nth @recThrArray index4)) :S)
       ]
       (if (and straight1 straight2 straight3 straight4 @turboAv)
            (if (> @NrRadius50 @NrRadius100) false true)
            false)))
;---------------------------------------------------------------;
(defn- realtime-straight [throttle angle]
  (let [thrVal (:value throttle)
        absAngle (* angle angle)
        arst (and (> @NrRadius100 @NrRadius50) (> @NrRadius100 @NrStraight))
        ;
        speedStraight (if arst 0.2 0.3)
        breakRadius (if arst 0.25 0.2)
        fullThrottle (if arst 0.8 1.0)
        smallRadius (if arst 400 600)
        ;
        index (:index throttle)
        index1 (mod (+ index 1) (count @recThrArray))
        index2 (mod (+ index 2) (count @recThrArray))
        index3 (mod (+ index 3) (count @recThrArray))
        straight1 (= (:type (nth @recThrArray index1)) :S)
        straight2 (= (:type (nth @recThrArray index2)) :S)
        straight3 (= (:type (nth @recThrArray index3)) :S)
        ;
        adjThrVal (if (and straight1 straight2 )
                      (+ thrVal speedStraight)
                      (if (not straight1)
                          (- thrVal breakRadius)
                          (+ thrVal speedStraight)))
       ]
       adjThrVal))
;---------------------------------------------------------------;
(defn- realtime-radius [throttle angle]
  (let [thrVal (:value throttle)
        absAngle (* angle angle)
        ;
        index (:index throttle)
        index1 (mod (+ index 1) (count @recThrArray))
        index2 (mod (+ index 2) (count @recThrArray))
        straight1 (= (:type (nth @recThrArray index1)) :S)
        straight2 (= (:type (nth @recThrArray index2)) :S)
        ;
        adjThrValR (- thrVal 0.1)
        bigAngleT (> absAngle 2800)
        smallAngleT (< absAngle (if (>= @NrStraight (+ @NrRadius100 @NrRadius50)) 2800 200))
        staightsBoost (if (>= @NrStraight (+ @NrRadius100 @NrRadius50)) 0.35 0.11)
        ;
        adjThrVal (if bigAngleT
                      adjThrValR
                      (if smallAngleT
                          (+ thrVal staightsBoost)
                          (if (and straight1 straight2) (+ thrVal 0.1) thrVal)))
       ]
       adjThrVal))
;---------------------------------------------------------------;
(defn- realtime-thr-calc [throttle angle]
  (let [straight (= (:type throttle) :S)
        value (if straight (realtime-straight throttle angle) (realtime-radius throttle angle))
        newThrottle (if (> value 1.0) 1.0 value)
       ]
    (if (realtime-straight-turbo throttle)
        (do (reset! turboAv false)
            (reset! turboOn true)
            {:msgType "turbo" :data "turbotastic"})
        (if @turboOn
            (let [_ (swap! turboTicks dec)
                  turboOff (<= @turboTicks 1)
                  _ (if turboOff (reset! turboOn false))
                  tac (/ @turboTicks @turboFactor)
                  multiply (if turboOff 1.0 tac)]
              {:msgType "throttle" :data (* newThrottle multiply)})
            {:msgType "throttle" :data newThrottle}))
  ))

;---------------------------------------------------------------;
;---------------------------------------------------------------;
(defmulti handle-msg :msgType)
(defmethod handle-msg "carPositions" [msg]
  (let [index (:pieceIndex (carPosition msg))
        staticVal (nth @recThrArray index)
        response (realtime-thr-calc staticVal (carAngle msg))
       ]
  response))
;---------------------------------------------------------------;
(defmethod handle-msg "gameInit" [msg]
  (let [_ (reset! recThrArray [])
        _ (reset! currIndex -1)
        pieceArray (:pieces (:track (:race (:data msg))))
        thrArray (static-thr-calc pieceArray)
       ])
  (create-ping)
)
;---------------------------------------------------------------;
(defmethod handle-msg "lapFinished" [msg]
  (let [_ (reset! currIndex -1)])
  (create-ping)
)
;---------------------------------------------------------------;
(defmethod handle-msg "turboAvailable" [msg]
  (let [_ (reset! turboAv true)
        _ (reset! turboOn false)
        _ (reset! turboFactor (:turboDurationTicks (:data msg)))        
        _ (reset! turboTicks (:turboDurationTicks (:data msg)))
        _ (println (:turboFactor (:data msg)) " for " @turboTicks )
       ])
  (create-ping)
)
;---------------------------------------------------------------;
(defmethod handle-msg "yourCar" [msg]
  (let [_ (reset! carColor (:color (:data msg)))
        _ (println @carColor)
       ])
  (create-ping)
)
;---------------------------------------------------------------;
(defmethod handle-msg :default [msg]
  (create-ping))

;---------------------------------------------------------------;
;---------------------------------------------------------------;
(defn game-loop [channel]
  (let [msg (read-message channel)]
    (log-msg msg)
    (send-message channel (handle-msg msg))
    (recur channel)))
(defn -main[& [host port botname botkey track]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (if track
        (send-message channel {:msgType "joinRace" :data { :botId {:name botname :key botkey} :trackName track :carCount 1}})
        (send-message channel {:msgType "join" :data {:name botname :key botkey}} ))
    (game-loop channel)))

